#include <Adafruit_BME280.h>
#include <Adafruit_GFX.h>
#include <Adafruit_I2CDevice.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_Sensor.h>
#include <Arduino.h>
#include <ESP8266WebServer.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "code_secrets.h"

// Set WiFi credentials
#define wifi_ssid SECRET_WIFI_SSID
#define wifi_password SECRET_WIFI_PASS

// Set MQTT endpoint
#define mqtt_server SECRET_MQTT_SERVER
#define mqtt_port SECRET_MQTT_PORT
#define mqtt_user SECRET_MQTT_USER
#define mqtt_pass SECRET_MQTT_PASS

// Set MQTT topics
#define sensor_name "rancho"
#define sensor_location "local"
#define topic_temperature sensor_name "/" sensor_location "/temperature"
#define topic_humidity sensor_name "/" sensor_location "/humidity"
#define topic_pressure sensor_name "/" sensor_location "/pressure"
#define topic_acvoltage sensor_name "/" sensor_location "/acvoltage"

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 64
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);

// Declare sensors and objects
WiFiClient espClient;
PubSubClient mqttClient(espClient);
Adafruit_BME280 bme;

int data_update_timer = 5;
int info_display_time = 3;
int nextGet, display_index, show_screen;

unsigned long connect_timeout = 30;
unsigned long timeNow = 0;
unsigned long timeDelta = 0;

// Set variables
float temperature, humidity, pressure, acvoltage;

// Forward function declaration
bool setup_wifi();
void reconnect_mqtt();
void get_sensor_data();
void get_ac_data();
bool publish_topic(const char *topic, const char *payload);
int display_time(int timer, int index);
int center_print(String print_value, int text_size, int display_width_pixels);
void display_data(String key, float value);

void setup() {
  Serial.begin(115200);

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;
  }

  bme.begin(0x76);

  get_sensor_data();
  nextGet = data_update_timer;
  show_screen = info_display_time;
}

void loop() {

  if (nextGet > 0) {

    nextGet -= 1;

  } else {

    get_sensor_data();
    get_ac_data();

    bool network = setup_wifi();

    if (network == true) {

      mqttClient.setServer(mqtt_server, mqtt_port);

      if (!mqttClient.connected()) {
        reconnect_mqtt();
      }

      mqttClient.loop();

      display.clearDisplay();
      display.setTextSize(1);
      display.setTextColor(WHITE, BLACK);
      display.setCursor(0, 0);
      display.println("Publish to MQTT...");

      bool pub_temp =
          publish_topic(topic_temperature, String(temperature).c_str());
      if (pub_temp == 1) {
        display.setCursor(0, 8);
        display.println("- Temperature OK");
      } else {
        display.setCursor(0, 8);
        display.println("- Temperature FAIL");
      }

      bool pub_hum = publish_topic(topic_humidity, String(humidity).c_str());
      if (pub_hum == 1) {
        display.setCursor(0, 16);
        display.println("- Humidity OK");
      } else {
        display.setCursor(0, 16);
        display.println("- Humidity FAIL");
      }

      bool pub_press = publish_topic(topic_pressure, String(pressure).c_str());
      if (pub_press == 1) {
        display.setCursor(0, 24);
        display.println("- Pressure OK");
      } else {
        display.setCursor(0, 24);
        display.println("- Pressure FAIL");
      }

      bool pub_volt = publish_topic(topic_acvoltage, String(acvoltage).c_str());
      if (pub_volt == 1) {
        display.setCursor(0, 32);
        display.println("- Voltage OK");
      } else {
        display.setCursor(0, 32);
        display.println("- Voltage FAIL");
      }

      display.display();

      delay(5000);

      mqttClient.disconnect();
    }

    nextGet = data_update_timer;
  }

  display.clearDisplay();
  display.setTextColor(WHITE, BLACK);

  if (show_screen > 0) {
    show_screen -= 1;
  } else {
    display_index += 1;
    show_screen = info_display_time;
  }

  if (display_index == 0) {
    display_data("DEGREES", temperature);
  } else if (display_index == 1) {
    display_data("HUMIDITY", humidity);
  } else if (display_index == 2) {
    display_data("PRESSURE", pressure);
  } else if (display_index == 3) {
    display_data("AC VOLTAGE", acvoltage);
  } else if (display_index == 4) {
    display_data("UPDATE IN", nextGet);
  } else {
    display_index = 0;
  }
  display.display();

  delay(1000);
}

bool setup_wifi() {

  timeNow = millis() / 1000;

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE, BLACK);
  display.setCursor(0, 0);
  display.println("WiFi SSID:");
  display.setCursor(65, 0);
  display.println(wifi_ssid);
  display.setCursor(0, 8);
  display.println("Connecting");
  int dot_position = 55;
  int dot_row = 8;

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {

    timeDelta = millis() / 1000;

    dot_position = dot_position + 5;
    if (dot_position == 125) {
      dot_position = 0;
      dot_row = dot_row + 8;
    }

    if (timeDelta - timeNow <= connect_timeout) {
      display.setCursor(dot_position, dot_row);
      display.println(".");
      display.display();
    } else {
      int timeout_row = dot_row + 8;
      display.setCursor(0, timeout_row);
      display.println("Timeout (" + String(connect_timeout) + "s)");
      timeout_row += 8;
      display.setCursor(0, timeout_row);
      display.println("Skip data upload.");
      display.display();
      delay(3000);
      return false;
    }
    delay(1000);
  }

  int connect_row = dot_row + 8;
  display.setCursor(0, connect_row);
  display.println("Connected!");
  display.display();
  delay(1000);
  return true;
}

void reconnect_mqtt() {
  timeNow = millis() / 1000;
  int dot_position = 90;

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE, BLACK);

  while (!mqttClient.connected()) {

    display.setCursor(0, 0);
    display.println("Connect to MQTT");
    display.display();

    if (mqttClient.connect("ESP8266Client", mqtt_user, mqtt_pass)) {

      display.setCursor(90, 0);
      display.println(" OK   ");
      display.display();
      delay(1000);

    } else {
      timeDelta = millis() / 1000;
      if (timeDelta - timeNow >= connect_timeout) {

        display.setCursor(0, 8);
        display.println("Timeout (" + String(connect_timeout) + "s)");
        display.setCursor(0, 16);
        display.println("Skip data upload.");
        display.display();
        delay(3000);
        return;
      }

      display.setCursor(dot_position, 0);
      display.println(".");
      display.display();
      dot_position += 6;

      delay(5000);
    }
  }
}

void get_sensor_data() {
  temperature = bme.readTemperature();
  humidity = bme.readHumidity();
  pressure = bme.readPressure() / 100.0F;
}

void get_ac_data() {
  int sensorValue = analogRead(A0);
  acvoltage = sensorValue * (250.0 / 1024.0);
}

bool publish_topic(const char *topic, const char *payload) {
  bool publish = mqttClient.publish(topic, payload, true);
  return publish;
}

int center_print(String print_value, int text_size, int display_width_pixels) {
  int char_width, x_pos, print_pixels;
  if (text_size == 1) {
    char_width = 6;
  } else if (text_size == 2) {
    char_width = 12;
  } else if (text_size == 3) {
    char_width = 18;
  }
  int print_chars = print_value.length();
  print_pixels = print_chars * char_width;
  x_pos = (display_width_pixels - print_pixels) / 2;
  return x_pos;
}

void display_data(String key, float value) {
  int x;
  int text_size = 2;
  int data_size = 3;
  x = center_print(key, text_size, 128);
  display.setTextSize(text_size);
  display.setCursor(x, 0);
  display.println(key);
  x = center_print(String(value), data_size, 128);
  display.setTextSize(data_size);
  display.setCursor(x, 30);
  display.println(value);
}
